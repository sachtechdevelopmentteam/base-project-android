package com.sachtech.storex.util.recyclerview

interface ItemClickListener<T> {
    fun onItemClick(item: T)
}