package com.sachtech.storex.util.rxkotlin

import io.reactivex.FlowableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by Akash Saggu(R4X) on 13-04-2018.
 */
object Transformers {

    fun <T> applyItoMonO(): ObservableTransformer<T, T> {

        return ObservableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(40, TimeUnit.SECONDS)
                .retry(2)
        }

    }

    fun <T> applyCtoMonO(): ObservableTransformer<T, T> {

        return ObservableTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(40, TimeUnit.SECONDS)
                .retry(2)
        }

    }

    fun <T> applyCtoIonO(): ObservableTransformer<T, T> {

        return ObservableTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.io())

        }

    }


    fun <T> applyItoMonF(): FlowableTransformer<T, T> {

        return FlowableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(2)
        }

    }

    fun <T> applyCtoMonF(): FlowableTransformer<T, T> {

        return FlowableTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(2)
        }

    }

    fun <T> applyItoMonS(): SingleTransformer<T, T> {

        return SingleTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(2)
        }

    }

    fun <T> applyCtoMonS(): SingleTransformer<T, T> {

        return SingleTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(40, TimeUnit.SECONDS)
                .retry(2)
        }

    }

}