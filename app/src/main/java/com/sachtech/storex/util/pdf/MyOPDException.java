package com.sachtech.storex.util.pdf;

/**
 * Created by Akash Saggu(R4X) on 2/13/19 at 10:26 AM
 * akashsaggu@protonmail.com
 *
 * @Version 1 2/13/19
 * @Updated on 2/13/19
 */
class MyOPDException extends Exception {

    String excep = "";

    public MyOPDException(String exception) {
        excep = exception;
    }

    @Override
    public String getLocalizedMessage() {
        return excep;
    }

    @Override
    public String getMessage() {
        return excep;
    }
}
