package com.sachtech.storex.util.extension

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.support.annotation.ColorInt
import android.support.v4.content.FileProvider
import android.view.View
import android.view.animation.PathInterpolator
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.io.File


/**
 * Created by Akash Saggu(R4X) on 12/9/18 at 18:54.
 * akashsaggu@protonmail.com
 * @Version 1 (12/9/18)
 * @Updated on 12/9/18
 */

inline fun <reified T> ImageView.loadImage(source: T) {
    when (T::class) {
        String::class -> Glide.with(this.context)
            .load(source)
            .apply(requestOptions())
            .into(this)
        Uri::class -> Glide.with(this.context)
            .load(source as Uri)
            .into(this)
        else -> Glide.with(this.context)
            .load(source)
            .into(this)
    }

}

fun ImageView.requestOptions(): RequestOptions {
    return RequestOptions()
        .useUnlimitedSourceGeneratorsPool(true)
        //.diskCacheStrategy(DiskCacheStrategy.NONE)
        //.skipMemoryCache(true)
        .override(width, height)
}

/*
inline fun palletListener(view: ImageView, crossinline body: (Palette) -> Unit): BitmapImageViewTarget {
    return object : BitmapImageViewTarget(view) {
        override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {
            super.onResourceReady(resource, transition)
            Palette.from(resource).generate { palette: Palette ->
                body.invoke(palette)
            }
        }
    }
}*/

fun View.createBackgroundColorTransition(@ColorInt startColor: Int, @ColorInt endColor: Int): Animator {
    return createColorAnimator(this, "backgroundColor", startColor, endColor)
}

fun TextView.createTextColorTransition(@ColorInt startColor: Int, @ColorInt endColor: Int): Animator {
    return createColorAnimator(this, "textColor", startColor, endColor)
}

private fun createColorAnimator(
    target: Any,
    propertyName: String, @ColorInt startColor: Int, @ColorInt endColor: Int
): Animator {
    val animator: ObjectAnimator
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        animator = ObjectAnimator.ofArgb(target, propertyName, startColor, endColor)
    } else {
        animator = ObjectAnimator.ofInt(target, propertyName, startColor, endColor)
        animator.setEvaluator(ArgbEvaluator())
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        animator.interpolator = PathInterpolator(0.4f, 0f, 1f, 1f)
    }
    animator.duration = 1000
    return animator
}

/*
fun Context.selectImageFromGallery(intent: Intent, imageView: ImageView): String? {
    val selectedImage = intent?.data
    val filePathColumn = arrayOf<String>(MediaStore.Images.Media.DATA)
    val cursor = this?.contentResolver?.query(
        selectedImage,
        filePathColumn, null, null, null
    )
    cursor?.moveToFirst()
    val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
    val picturePath = columnIndex?.let { cursor?.getString(it) }
    var file = File(picturePath)
    val pictureUri = FileProvider.getUriForFile(this, getString(R.string.file_provider_authority), file)
    cursor?.close()
    imageView.setImageURI(pictureUri)
    return picturePath
}
*/
