package com.sachtech.storex.networking.helper

import java.io.IOException

/**
 * Created by Akash Saggu(R4X) on 24-04-2018.
 */
class NoNetworkException : IOException() {
    override fun getLocalizedMessage(): String {
        return "No connectivity exception"
    }
}