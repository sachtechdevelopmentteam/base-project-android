package com.sachtech.storex.networking.helper

import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


class ResponseConsumer<T>(val onRequestSuccess: (T) -> Unit, val onRequestError: (String) -> Unit) : SingleObserver<T> {

    override fun onSuccess(t: T) {
        onRequestSuccess(t)
    }

    override fun onSubscribe(d: Disposable) {}

    override fun onError(e: Throwable) {
        if (e is HttpException) {
            val responseBody = e.response().errorBody()
            onRequestError(getErrorMessage(responseBody!!))
        } else if (e is SocketTimeoutException) {
            onRequestError("Request Timeout")
        } else if (e is IOException) {
            onRequestError("Network Error")
        } else {
            onRequestError(e.localizedMessage)
        }
    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("Message")
        } catch (e: Exception) {
            e.message!!
        }
    }
}