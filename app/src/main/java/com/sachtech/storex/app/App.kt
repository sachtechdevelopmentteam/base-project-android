package com.sachtech.storex.app

import android.app.Application
import com.sachtech.storex.di.viewModel
import com.sachtech.storex.util.preference.SharedPreferenceUtils
import org.koin.android.ext.android.startKoin


/**
 * Created by Akash Saggu(R4X)
 */

class App : Application() {

    val preference by lazy { SharedPreferenceUtils(this) }

    override fun onCreate() {
        super.onCreate()
        application = this
        startKoin(this, listOf(viewModel))
    }

    companion object {
        lateinit var application: App
        @JvmStatic
        fun getApp() = application
    }

}

fun Any.getPref(): SharedPreferenceUtils {
    return App.getApp().preference
}