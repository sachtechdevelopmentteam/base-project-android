package com.sachtech.storex.database.dao

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Update

interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<T>)

    @Update
    fun update(data: T)

    @Delete
    fun delete(data: T)

    @Delete
    fun deleteMultiple(data: List<T>)

    @Delete
    fun deleteList(data: List<T>)
}