package com.sachtech.storex.database.helper

import com.sachtech.storex.database.RuthDatabase
import com.sachtech.storex.database.dao.UserDao

object DatabaseHelper {

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T> getDao(): T? {
        return when (T::class) {
            UserDao::class -> {
                RuthDatabase.getInstance()!!.userDao() as T
            }
            else -> return null
        }
    }
}
