package com.sachtech.storex.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.sachtech.storex.database.entity.User

/**
 * Created by Akash Saggu(R4X) on 31/8/18 at 15:52.
 * akashsaggu@protonmail.com
 * @Version 1
 * @Updated on 31/8/18
 */

@Dao
interface UserDao {

    @Query("SELECT * FROM user where id = 0")
    fun getUser(): User?

}