package com.sachtech.storex.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.sachtech.storex.app.App
import com.sachtech.storex.database.dao.UserDao
import com.sachtech.storex.database.entity.User
import student.bac.edu.my.database.typeconverter.LoginTypeConverter

@Database(entities = [User::class], version = 1)
@TypeConverters(value = [LoginTypeConverter::class])
abstract class RuthDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao


    companion object {
        private var INSTANCE: RuthDatabase? = null
        fun getInstance(): RuthDatabase? {
            if (INSTANCE == null) {
                synchronized(RuthDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        App.getApp(),
                        RuthDatabase::class.java, "ruth.db"
                    )
                        /*.openHelperFactory(factory)*/
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}