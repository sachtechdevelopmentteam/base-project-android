package com.sachtech.storex.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Akash Saggu(R4X) on 31/8/18 at 15:06.
 * akashsaggu@protonmail.com
 * @Version 1
 * @Updated on 9/11/18
 */

@Entity(tableName = "user")
data class User(
    @ColumnInfo(name = "id") @PrimaryKey val id: String = "0",
    @ColumnInfo(name = "name") @PrimaryKey val name: String = "",
    @ColumnInfo(name = "email") @PrimaryKey val email: String = "",
    @ColumnInfo(name = "phone") @PrimaryKey val phone: String = "",
    @ColumnInfo(name = "socialId") @PrimaryKey val socialId: String = "",
    @ColumnInfo(name = "loginType", typeAffinity = ColumnInfo.BLOB) @PrimaryKey val loginType: LoginType
)

enum class LoginType {
    Facebook,
    Google,
    Instagram,
    WeChat,
    Email,
    Phone
}