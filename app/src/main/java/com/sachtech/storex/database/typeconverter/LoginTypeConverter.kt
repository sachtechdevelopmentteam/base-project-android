package student.bac.edu.my.database.typeconverter

import com.sachtech.storex.database.entity.LoginType
import student.bac.edu.my.database.typeconverter.base.ObjectConverter

/**
 * Created by Akash Saggu(R4X) on 6/8/18 at 15:04.
 * akashsaggu@protonmail.com
 * @Version 1
 */
object LoginTypeConverter : ObjectConverter<LoginType>()