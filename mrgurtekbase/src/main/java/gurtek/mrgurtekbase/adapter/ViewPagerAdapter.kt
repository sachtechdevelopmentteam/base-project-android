package gurtek.mrgurtekbase.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Created by Akash Saggu(R4X) on 17-02-2018.
 */
class ViewPagerAdapter(fragmentManager: FragmentManager, val fragments: ArrayList<Fragment>, val titles: ArrayList<String> = arrayListOf(), val showTitle: Boolean = false) : FragmentStatePagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun addFragment(fragment: Fragment, title: String = "") {
        fragments.add(fragment)
        titles.add(title)
    }
    override fun getPageTitle(position: Int): CharSequence? {
        return if (showTitle) {
            titles[position]
        } else
            super.getPageTitle(position)
    }

}