package gurtek.mrgurtekbase.adapter

interface BaseView {
    fun showErrorMessage(message: String)
    fun showProgress()
    fun hideProgress()
    fun onBackPress()
}