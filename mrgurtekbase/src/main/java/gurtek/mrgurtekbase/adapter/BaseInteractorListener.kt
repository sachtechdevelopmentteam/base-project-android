package gurtek.mrgurtekbase.adapter

interface BaseInteractorListener {
    fun onError(error: String)
}